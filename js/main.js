(function ($) {
 Drupal.behaviors.yg_solid = {
   attach: function (context, settings) {

	'use strict';

	new WOW().init();
	// iPad and iPod detection	
	var isiPad = function(){
		return (navigator.platform.indexOf("iPad") != -1);
	};

	var isiPhone = function(){
	    return (
			(navigator.platform.indexOf("iPhone") != -1) || 
			(navigator.platform.indexOf("iPod") != -1)
	    );
	};

	// Parallax
	var parallax = function() {
		$(window).stellar();
	};



	// Burger Menu
	var burgerMenu = function() {

		$('body').on('click', '.js-fh5co-nav-toggle', function(event){

			event.preventDefault();

			if ( $('#navbar').is(':visible') ) {
				$(this).removeClass('active');
			} else {
				$(this).addClass('active');	
			}

		});

	};


	
	// Page Nav
	var clickMenu = function() {
if (!$("body").hasClass("path-frontpage")) {
            $("#navbar li a").each(function() {
                $(this).attr("href", "/yg_solid/");
            });
        }
        else{
			$('#navbar a:not([class="external"])').click(function(event){
				var section = $(this).data('nav-section'),
					navbar = $('#navbar');

				    	$('html, body').animate({
				        	scrollTop: $('[data-section="' + section + '"]').offset().top - 5
				    	}, 400);
				    	
				
			    if ( navbar.is(':visible')) {
			    	navbar.removeClass('in');
			    	navbar.attr('aria-expanded', 'false');
			    	$('.js-fh5co-nav-toggle').removeClass('active');
			    }

			    event.preventDefault();
			    return false;
			});

        }

	};

	// Reflect scrolling in navigation
	var navActive = function(section) {

		var $el = $('#navbar > ul');
		$el.find('li').removeClass('active');
		$el.each(function(){
			$(this).find('a[data-nav-section="'+section+'"]').closest('li').addClass('active');
		});

	};

	var navigationSection = function() {

		var $section = $('section[data-section]');
		
		$section.waypoint(function(direction) {
		  	
		  	if (direction === 'down') {
		    	navActive($(this.element).data('section'));
		  	}
		}, {
	  		offset: '150px'
		});

		$section.waypoint(function(direction) {
		  	if (direction === 'up') {
		    	navActive($(this.element).data('section'));
		  	}
		}, {
		  	offset: function() { return -$(this.element).height() + 155; }
		});

	};

	// Window Scroll
	var windowScroll = function() {
		var lastScrollTop = 0;

		$(window).scroll(function(event){

		   	var header = $('#fh5co-header'),
				scrlTop = $(this).scrollTop();

			if ( scrlTop > 500 && scrlTop <= 2000 ) {
				header.addClass('navbar-fixed-top fh5co-animated slideInDown');
			} else if ( scrlTop <= 500) {
				if ( header.hasClass('navbar-fixed-top') ) {
					header.addClass('navbar-fixed-top fh5co-animated slideOutUp');
					setTimeout(function(){
						header.removeClass('navbar-fixed-top fh5co-animated slideInDown slideOutUp');
					},1 );
				}
			} 
			
		});
	};

	var counter = function() {
		$('.js-counter').countTo({
			 formatter: function (value, options) {
	      return value.toFixed(options.decimals);
	    },
		});
	};

	var counterWayPoint = function() {
		if ($('#fh5co-counter-section').length > 0 ) {
			$('#fh5co-counter-section').waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {
					setTimeout( counter , 400);					
					$(this.element).addClass('animated');
				}
			} , { offset: '90%' } );
		}
	};


	$('#fh5co-testimony .grid').masonry({
	  itemSelector: '.col-md-4',
	  columnWidth: '.col-md-4',
	  percentPosition: true
	});

	$(".date").html(function(){
	  var text= $(this).text().trim().split(" ");
	  var last = text.pop();
	  var first = text.shift();
	  return  "<span>"+ first + "</span> <small>" + last + "</small>";
	});

	// Document on load.
	$(function(){
		// parallax();
		burgerMenu();
		clickMenu();
		// windowScroll();
		navigationSection();
		counterWayPoint();

	});

	



}}})(jQuery, Drupal);// End of use strict
